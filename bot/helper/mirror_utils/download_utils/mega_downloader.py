from random import *
from string import *
from os import *
from threading import *
from mega import *
from bot import *
from bot.helper.telegram_helper.message_utils import *
from bot.helper.ext_utils.bot_utils import *
from bot.helper.mirror_utils.upload_utils.gdriveTools import *
from bot.helper.ext_utils.fs_utils import *
from bot.helper.mirror_utils.status_utils.mega_download_status import *


class MegaAppListener(MegaListener):
    return 



class AsyncExecutor:

    def __init__(self):
        self.continue_event = Event()

    def do(self, function, args):
        self.continue_event.clear()
        function(*args)
        self.continue_event.wait()


def add_mega_download(mega_link: str, path: str, listener, name: str):
    return 
    




    # def add_download(self, link, path):
    #     Path(path).mkdir(parents=True, exist_ok=True)
    #     try:
    #         dl = self.__mega_client.addDl(link, path)
    #     except Exception as err:
    #         LOGGER.error(err)
    #         return sendMessage(str(err), self.__listener.bot, self.__listener.message)
    #     gid = dl['gid']
    #     info = self.__mega_client.getDownloadInfo(gid)
    #     file_name = info['name']
    #     file_size = info['total_length']
    #     if STOP_DUPLICATE and not self.__listener.isLeech:
    #         LOGGER.info('Checking File/Folder if already in Drive')
    #         mname = file_name
    #         if self.__listener.isZip:
    #             mname = f"{mname}.zip"
    #         elif self.__listener.extract:
    #             try:
    #                 mname = get_base_name(mname)
    #             except:
    #                 mname = None
    #         if mname is not None:
    #             if TELEGRAPH_STYLE is True:
    #                 smsg, button = GoogleDriveHelper().drive_list(mname, True)
    #                 if smsg:
    #                     msg1 = "File/Folder is already available in Drive.\nHere are the search results:"
    #                     return sendMarkup(msg1, self.__listener.bot, self.__listener.message, button)
    #             else:
    #                 cap, f_name = GoogleDriveHelper().drive_list(mname, True)
    #                 if cap:
    #                     cap = f"File/Folder is already available in Drive. Here are the search results:\n\n{cap}"
    #                     sendFile(self.__listener.bot, self.__listener.message, f_name, cap)
    #                     return
    #     if any([STORAGE_THRESHOLD, ZIP_UNZIP_LIMIT, MEGA_LIMIT, LEECH_LIMIT]):
    #         arch = any([self.__listener.isZip, self.__listener.extract])
    #         if STORAGE_THRESHOLD is not None:
    #             acpt = check_storage_threshold(file_size, arch)
    #             if not acpt:
    #                 msg = f'You must leave {STORAGE_THRESHOLD}GB free storage.'
    #                 msg += f'\nYour File/Folder size is {get_readable_file_size(file_size)}'
    #                 return sendMessage(msg, self.__listener.bot, self.__listener.message)
    #         limit = None
    #         if ZIP_UNZIP_LIMIT is not None and arch:
    #             msg3 = f'Failed, Zip/Unzip limit is {ZIP_UNZIP_LIMIT}GB.\nYour File/Folder size is {get_readable_file_size(file_size)}.'
    #             limit = ZIP_UNZIP_LIMIT
    #         if LEECH_LIMIT is not None and self.__listener.isLeech:
    #             msg3 = f'Failed, Leech limit is {LEECH_LIMIT}GB.\nYour File/Folder size is {get_readable_file_size(file_size)}.'
    #             limit = LEECH_LIMIT
    #         elif MEGA_LIMIT is not None:
    #             msg3 = f'Failed, Mega limit is {MEGA_LIMIT}GB.\nYour File/Folder size is {get_readable_file_size(file_size)}.'
    #             limit = MEGA_LIMIT
    #         if limit is not None:
    #             LOGGER.info('Checking File/Folder Size...')
    #             if file_size > limit * 1024**3:
    #                 return sendMessage(msg3, self.__listener.bot, self.__listener.message)
    #     self.__onDownloadStart(file_name, file_size, gid)
    #     LOGGER.info(f'Mega download started with gid: {gid}')

    # def cancel_download(self):
    #     LOGGER.info(f'Cancelling download on user request: {self.gid}')
    #     self.__mega_client.cancelDl(self.gid)
